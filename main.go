package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var serverAddress = "ws://localhost:8080/ws"

func main() {
	var wg sync.WaitGroup

	aCh := make(chan string)
	bCh := make(chan string)

	wg.Add(2)
	go createClient(aCh, bCh, &wg)
	go createClient(bCh, aCh, &wg)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	wg.Wait()
}

func createClient(writeChan chan string, readChan chan string, wg *sync.WaitGroup) {
	defer wg.Done()

	for {
		conn, _, err := websocket.DefaultDialer.Dial(serverAddress, nil)
		if err != nil {
			fmt.Println("Connenction failed")
			time.Sleep(5 * time.Second)
			continue
		}
		go sendMessage(conn, writeChan)
		go readMessage(conn, readChan)
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c
		closeConn(conn)
	}
}

func closeConn(conn *websocket.Conn) {
	defer conn.Close()
	err := conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		fmt.Println("fail to close connection")
	}
}

func sendMessage(conn *websocket.Conn, writeChan chan string) {
	for {
		message := strconv.Itoa(rand.Intn(1000))
		err := conn.WriteMessage(websocket.TextMessage, []byte(message))
		if err != nil {
			fmt.Println("message send failed")
			return
		}
		writeChan <- message
		time.Sleep(1 * time.Second)
	}
}

func readMessage(conn *websocket.Conn, readChan chan string) {
	for {
		_, p, err := conn.ReadMessage()
		if err != nil {
			fmt.Println(err)
			return
		}
		message := <-readChan
		resultMessage := "Matched!"
		if message != string(p) {
			resultMessage = "Unmatch, received: " + string(p) + ", supposed: " + message
		}
		fmt.Println(resultMessage)
	}
}
